function problem4(inventory) {
  if (!inventory) return [];
  const carYears = [];
  for (let cars of inventory) {
    carYears.push(cars.car_year);
  }
  return carYears;
}

module.exports = problem4;