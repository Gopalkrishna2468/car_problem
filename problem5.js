const problem4 = require("./problem4");

function problem5(inventory, year) {
    if (!inventory || !year) return [];
    const years = problem4(inventory);
    const olderThan2000 = [];
    for (let cars of years) {
        if (cars < 2000) {
            olderThan2000.push(cars);
        }
    }
    return olderThan2000;
}

module.exports = problem5;