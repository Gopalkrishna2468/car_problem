function problem1(inventory, id) {
    if (!inventory || !id) return [];
    for (let cars of inventory) {
        if (cars.id === id) {
            return cars;
        }

    }
    return [];
}

module.exports = problem1;