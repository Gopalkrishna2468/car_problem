function problem6(inventory, opt) {
    if (!inventory || !opt) return [];
    const audibmwList = [];
    for (let cars of inventory) {
        for (let car of opt) {
            if (cars.car_make === car) {
                audibmwList.push(cars);
            }
        }
    }
    return audibmwList;
};

module.exports = problem6;