function problem3(inventory) {
    if (!inventory) return [];
    const allCarModels = [];
    for (let cars of inventory) {
        allCarModels.push(cars.car_model);
    }
    console.log(allCarModels);
    return allCarModels.sort();
}

module.exports = problem3;